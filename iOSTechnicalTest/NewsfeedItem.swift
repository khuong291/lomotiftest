//
//  NewsfeedItem.swift
//  iOSTechnicalTest
//
//  Created by KhuongPham on 9/3/19.
//  Copyright © 2019 Lomotif. All rights reserved.
//

import Foundation

struct NewsfeedItem: Codable {
    let previewURL: String
}
