//
//  PictureFeedCollectionViewCell.swift
//  iOSTechnicalTest
//
//  Created by KhuongPham on 9/2/19.
//  Copyright © 2019 Lomotif. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

final class PictureFeedCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
    var newsfeedItem: NewsfeedItem? {
        didSet {
            guard let newsfeedItem = newsfeedItem else {
                return
            }
            let url = URL(string: newsfeedItem.previewURL)
            imageView.kf.indicatorType = .activity
            imageView.kf.setImage(with: url)
        }
    }
}
