//
//  VideoPreviewViewController.swift
//  iOS Technical Test
//
//  Created by Casey Law on 11/12/18.
//  Copyright © 2018 Lomotif. All rights reserved.
//

import UIKit
import MBProgressHUD
import AVFoundation
import AVKit
import AssetsLibrary

class VideoPreviewViewController: UIViewController {

    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    
    var timer: Timer?
    var player: AVPlayer?
    var duration = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func startButtonTapped(_ sender: UIButton) {
        playMergedVideo()
    }
    
    private func playMergedVideo() {
        guard let audioURL = URL(string: "https://audio-ssl.itunes.apple.com/apple-assets-us-std-000001/AudioPreview122/v4/8a/dd/1f/8add1f4d-142c-1317-250d-ff6370962fb8/mzaf_7601694821840779604.plus.aac.p.m4a"), let path = Bundle.main.path(forResource: "Movie", ofType:"mp4") else {
            return
        }
        let videoURL = URL(fileURLWithPath: path)
        let progressHUD = MBProgressHUD.showAdded(to: view, animated: true)
        progressHUD.label.text = "Loading..."
        mergeVideoWithAudio(videoUrl: videoURL, audioUrl: audioURL, success: { [weak self] url in
            guard let self = self else {
                return
            }
            DispatchQueue.main.async {
                progressHUD.hide(animated: true)
                self.player = AVPlayer(url: url)
                let playerLayer = AVPlayerLayer(player: self.player)
                playerLayer.frame = self.videoView.bounds
                self.videoView.layer.addSublayer(playerLayer)
                self.player?.play()
                self.updateProgressView()
                self.durationLabel.text = String(format: "%.2fs", self.duration)
            }
        }) { error in
            print(error?.localizedDescription ?? "")
        }
    }
    
    private func mergeVideoWithAudio(videoUrl: URL, audioUrl: URL, success: @escaping ((URL) -> Void), failure: @escaping ((Error?) -> Void)) {
        let mixComposition = AVMutableComposition()
        var mutableCompositionVideoTrack: [AVMutableCompositionTrack] = []
        var mutableCompositionAudioTrack: [AVMutableCompositionTrack] = []
        let totalVideoCompositionInstruction = AVMutableVideoCompositionInstruction()
        
        let aVideoAsset = AVAsset(url: videoUrl)
        let aAudioAsset = AVAsset(url: audioUrl)
        
        if let videoTrack = mixComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid), let audioTrack = mixComposition.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid) {
            mutableCompositionVideoTrack.append(videoTrack)
            mutableCompositionAudioTrack.append(audioTrack)
            
            if let aVideoAssetTrack = aVideoAsset.tracks(withMediaType: .video).first, let aAudioAssetTrack = aAudioAsset.tracks(withMediaType: .audio).first {
                do {
                    try mutableCompositionVideoTrack.first?.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration), of: aVideoAssetTrack, at: CMTime.zero)
                    try mutableCompositionAudioTrack.first?.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration), of: aAudioAssetTrack, at: CMTime.zero)
                    videoTrack.preferredTransform = aVideoAssetTrack.preferredTransform
                } catch {
                    print(error)
                }
                
                totalVideoCompositionInstruction.timeRange = CMTimeRangeMake(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration)
            }
        }
        
        let mutableVideoComposition: AVMutableVideoComposition = AVMutableVideoComposition()
        mutableVideoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        let width = view.bounds.width
        let height = width * 9 / 16
        mutableVideoComposition.renderSize = CGSize(width: width, height: height)
        
        if let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
            let outputURL = URL(fileURLWithPath: documentsPath).appendingPathComponent("\("fileName").m4v")
            
            do {
                if FileManager.default.fileExists(atPath: outputURL.path) {
                    
                    try FileManager.default.removeItem(at: outputURL)
                }
            } catch { }
            
            duration = CMTimeGetSeconds(mixComposition.duration)
            
            if let exportSession = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality) {
                exportSession.outputURL = outputURL
                exportSession.outputFileType = AVFileType.mp4
                exportSession.shouldOptimizeForNetworkUse = true
                exportSession.exportAsynchronously(completionHandler: {
                    switch exportSession.status {
                    case .failed:
                        if let _error = exportSession.error {
                            failure(_error)
                        }
                    case .cancelled:
                        if let _error = exportSession.error {
                            failure(_error)
                        }
                    default:
                        success(outputURL)
                    }
                })
            } else {
                failure(nil)
            }
        }
    }
    
    private func updateProgressView() {
        timer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(update), userInfo: nil, repeats: true)
    }
    
    @objc func update() {
        guard let player = player, player.rate != 0 else {
            timer?.invalidate()
            timer = nil
            return
        }
        let currentTime = player.currentTime().seconds
        progressView.progress = Float(currentTime / duration)
    }
}
