//
//  PictureFeedCollectionViewController.swift
//  iOS Technical Test
//
//  Created by Casey Law on 11/12/18.
//  Copyright © 2018 Lomotif. All rights reserved.
//

import UIKit

typealias JSON = [String: Any]

private let reuseIdentifier = "PictureFeedCollectionViewCell"

final class PictureFeedCollectionViewController: UICollectionViewController {
    
    fileprivate var newsfeed: Newsfeed?
    fileprivate let perPage = 20
    fileprivate var page = 1

    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        getData()
    }
    
    private func getData() {
        guard let url = URL(string: "https://pixabay.com/api/?key=10961674-bf47eb00b05f514cdd08f6e11&q=flower&per_page=\(perPage)&page=\(page)") else {
            return
        }
        let request = URLRequest(url: url)
        let dataTask = URLSession.shared.dataTask(with: request) { [weak self] data, response, error in
            guard let self = self else {
                return
            }
            do {
                if let data = data {
                    let decoder = JSONDecoder()
                    let newsfeed = try decoder.decode(Newsfeed.self, from: data)
                    if self.newsfeed == nil {
                        self.newsfeed = newsfeed // First load
                    } else {
                        self.newsfeed?.hits.append(contentsOf: newsfeed.hits) // Load more
                    }
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        dataTask.resume()
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let newsfeed = newsfeed else {
            return 0
        }
        return newsfeed.hits.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let newsfeed = newsfeed else {
            return UICollectionViewCell()
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! PictureFeedCollectionViewCell
        let newsfeedItem = newsfeed.hits[indexPath.row]
        cell.newsfeedItem = newsfeedItem
        return cell
    }
}

extension PictureFeedCollectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.bounds.width - 6) / 3
        return CGSize(width: width, height: width)
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let newsfeed = newsfeed else {
            return
        }
        let count = newsfeed.hits.count
        if count > 0 && indexPath.row == count - 1 && page < 4 {
            page += 1
            getData()
        }
    }
}
